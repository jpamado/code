#include <stdio.h>

int main() {

    int length;
    int height;
    int depth;
    printf("height:");
    scanf("%d", &height);
    printf("length:");
    scanf("%d", &length);
    printf("depth:");
    scanf("%d", &depth);

    int row;
    int column;

    for (row=0;row<=(height+depth);row++){
        for (column=0;column<=(length+depth);column++){
            if (( row==0 || row==height ) && column<=length ){
                printf("*");
            }else if (( column==0 || column==length ) && row <= height){
                printf("*");
            }else if (( column==row || column+height==row) && column<depth ){
                printf("*");
            }else if (( row==depth || row==height+depth ) && column>=depth ){
                printf("*");
            }else if (( column==depth || column==length+depth ) && row>=depth ){
                printf("*");
            }else if (( column+height==row+length || column==row+length ) && column>length ){
                printf("*");
            }else {
                printf(" ");
            }
        }
        printf("\n");
    }

    return 0;
}
