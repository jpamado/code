#include<stdio.h>
#include<stdlib.h>

int nCr(int n, int r);
int nCrModm (int n, int r, int m);
int isqrt(int n);
int isPrimeArr(int n);
int isPrimeDiv(int n);
int isPrimeCom(int n);
int isPrimeDivArr(int n);
int gcd(int a,int b);
int lcm(int a,int b);
int min(int a, int b);
int isPrimeChoose(int n);
int goodisqrt(int n);


int main(void){
    int i,sum = 0;

    for(i=900000000;i<1000000001;i++){
        if(isPrimeDivArr(i))
            sum++;
        if(i%1000000==0)
            printf("%d/%d\n",sum,i);
    }
    return 0;
}


//Space Complexity: O(n)
//Time Complexity: O(n^2)
int isPrimeChoose(int n){
    if (n == 2)
        return 1;
    if(n%2==0 || n < 2)
        return 0;

    int r = (n/2) + 1;

    int ** matrix = malloc(sizeof(int*)*(2));
    int i,j;
    for (i = 0;i < 2;i++)
        matrix[i] = malloc(sizeof(int)*(r+1));

    for(i = 0;i < 2;i++)
        for(j = 0;j <= r ;j++)
            if(j == 0 || j%2 == i)
                matrix[i%2][j] = 1;
            else
                matrix[i%2][j] = 0;

    for(i = 1;i <= n;i++){
        for(j = 1;(j<i) && (j<=r);j++)
            matrix[i%2][j] = (matrix[(i-1)%2][j] + matrix[(i-1)%2][j-1])%n;
        //if(i%500 == 0)
            //printf("%d/%d\n",i,n);
    }
    int sum = 0;
    i++;
    for(j = 1;j <= r;j++){
        sum += matrix[i%2][j];
    }

    for(i = 0;i < 2;i++){
        free(matrix[i]);
    }
    free(matrix);

    return (sum == 0);

}

int nCrModm(int n, int r, int m){
    if(r > (n/2))
        r = n - r;

    int ** matrix = malloc(sizeof(int*)*(2));
    int i,j;
    for (i = 0;i < 2;i++)
        matrix[i] = malloc(sizeof(int)*(r+1));

    for(i = 0;i < 2;i++)
        for(j = 0;j <= r ;j++)
            if(j == 0 || j%2 == i)
                matrix[i%2][j] = 1;
            else
                matrix[i%2][j] = 0;

    for(i = 1;i <= n;i++)
        for(j = 1;(j<i) && (j<=r);j++)
            matrix[i%2][j] = (matrix[(i-1)%2][j] + matrix[(i-1)%2][j-1])%m;

    int result = matrix[n%2][r];
    for(i = 0;i < 2;i++){
        free(matrix[i]);
    }
    free(matrix);
    return result;
}




int isPrimeArr(int n){
    if(n < 137){
        int primeArr[32] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131};
        int hi=31,lo=0;
        while(hi-lo > 1){
            if(n == primeArr[(hi+lo)/2])
                return 1;
            if(n < primeArr[(hi+lo)/2])
                hi = ((hi+lo)/2)-1;
            if(n > primeArr[(hi+lo)/2])
                lo = ((hi+lo)/2)+1;
        }
    }
    return 0;
}

//Time Complexity: O(sqrtn)
int isqrt(int n){
    int i=0;
    while(i*i<n)
        i++;
    return i;
}

int goodisqrt(int n){
    if(n < 3)
        return n;
    int i = 2;
    int mid,j = 46340;
    while(i*i<n && j*j>n && j!=i+1){
        mid = (i+j)/2;
        if(mid*mid > n){
            j = mid;
        }else{
            i = mid;
        }
    }
    return (i*i == n) ? i : i+1;
}

int isPrimeDiv(int n){
    if(n < 2)
        return 0;
    if(n == 2)
        return 1;
    if(!(n%2))
        return 0;
    int i,check = isqrt(n);
    for(i = 3 ; i <= check ; i += 2){
        if(n%i == 0)
            return 0;
    }
    return 1;
}


int isPrimeDivArr(int n){
    if(n < 137){
        if(n < 2)
            return 0;
        int primeArr[32] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131};
        int hi=31,lo=0;
        while(hi-lo > 1){
            if(n == primeArr[(hi+lo)/2])
                return 1;
            if(n < primeArr[(hi+lo)/2])
                hi = ((hi+lo)/2)-1;
            if(n > primeArr[(hi+lo)/2])
                lo = ((hi+lo)/2)+1;
        }
        return 0;
    }
    int primeArr[32] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131};
    int i;
    for(i = 0 ; i < 32 ; i++){
        if(n % primeArr[i] == 0)
            return 0;
    }
    int check = goodisqrt(n);
    for(i = 137 ; i <= check ; i += 6){
        if(n%i == 0 || n%(i+2) == 0)
            return 0;
        //printf("%d/%d",i,check);
    }
    return 1;
}





int min(int a, int b){
    if(a<b)
        return a;
    return b;
}

int gcd (int a, int b){
    if(a == b){
        if(a == 0)
            return -1;
        return a;
    }
    if(a < b)
        return gcd(b,a);
    if(a%b == 0)
        return b;
    return gcd(b,a%b);
}

int lcm(int a,int b){
    if(a == 0||b == 0)
        return -1;
    return (a*b)/(gcd(a,b));
}
