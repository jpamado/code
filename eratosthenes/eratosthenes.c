#include<stdio.h>

int isqrt (int x){
    int i;
    for(i=1;1;i++)
        if((i*i)>=x)
        return i;
}

int isPrime (int x){
    if(x==2||x==3||x==5||x==7)
        return 1;
    if(x==0||x==1||x%2==0||x%3==0)
        return 0;

    int check=isqrt(x);
    if(check%2==0)
        check--;
    int prime;

    for(prime=1;prime;check-=2)
        if(x%check==0)
            prime=0;

    if (check!=-1)
        return 0;
    return 1;
}

int main() {

    int N=0;
    while(N!=-1){
        printf("N = ");
        scanf("%d", &N);

        int i=0;
        int PiR=-1;
        while(PiR!=0){
            PiR=0;
            int j;
            for (j=0;j<N;j++){
                if(isPrime(N*i+j)){
                    PiR++;
                }
            }
            printf("\n%d , %d",i,PiR);
            if (PiR==0)
                break;
            i++;
        }
        printf("\n");
        printf("f(%d)=%d\n",N,i);
    }
    return 0;
}
