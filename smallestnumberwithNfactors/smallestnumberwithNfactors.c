#include<stdio.h>

int isqrt (int x){
    int i;
    for(i=1;1;i++)
        if((i*i)>x)
        return i-1;
}

int factNum (int x){
    int check=isqrt(x);
    int i;
    int factors=0;
    for(i=1;i<=check;i++){
        if(x%i==0)
            factors+=2;
        if(i*i==x)
            factors--;
    }
    return factors;
}

int main() {

    int N;
    printf("N = ");
    scanf("%d", &N);
    int i;
    int j;
    for(i=1;i<=N;i++){
        for(j=1;i!=factNum(j);j++){
            //nothing
        }
        printf("%d %d\n",i,j);
    }
    return 0;
}
