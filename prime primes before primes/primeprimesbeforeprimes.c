#include<stdio.h>

// Precondition: value will be an integer greater than 1.
// Postcondition: Returns 1 if value is prime and 0 otherwise.
int isPrime (int x){

    int check=x/2;
    int prime=1;

    for(check;prime!=0;check--)
        if(x%check==0)
            prime=0;

    if (check!=0)
        return 0;
    return 1;
}

// Precondition: x will be a positive integer greater than 1.
// Postcondition: Returns the next prime number greater than x.
int nextPrime (int x){
    x++;
    while ( isPrime(x)==0 )
        x++;
    return x;
}

int primesBefore (int num){
    int i=2;
    int count=0;
    while (i<num){
        i=nextPrime(i);
        count++;
    }
    return count;
}

int main(){

    int i=2;
    int n;
    while (i<10){
        n=primesBefore(i);
        if (isPrime(n)){
            printf("%d",i);
        }
        i=nextPrime(i);
    }

    return 0;
}
