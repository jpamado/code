//Juan Amado
//11/22/15
//boggle

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LENGTH 30

typedef struct trie {
    int isWord;
    struct trie* nextLetter [26];
} trie;

trie* insertWord(trie* dict, char word[], int k);
trie* init();
void wordStartingWith(char board[4][4], int used[4][4], char word[17], int x, int y, trie* dict);
int hasWord(char* word, trie* dict);
void freeDict(trie* dict);

int main() {
    //opens file with dictionary
    FILE* dictFile = fopen("dictionary.txt" , "r");
    if (dictFile == NULL) {
        printf("dictionary.txt not found\n");
        return 1;
    }
    //scans in number of words in the dictionary
    int dictWords,i;
    fscanf(dictFile , "%d" , &dictWords);

    //initializes the trie that the dictionary will be stored in
    trie* dict = init();

    //temporary sting for putting words into the trie
    char dictWord[MAX_LENGTH];

    for(i=0;i<dictWords;i++){
        //scans in a word
        fscanf(dictFile , "%s" , dictWord);
        int wordleng = strlen(dictWord);
        //if it is boggle legal
        if(wordleng <= 16 && wordleng >= 3)
            //add it to the dictionary
            dict = insertWord(dict, dictWord, 0);
    }
    //close the dictionary file
    fclose(dictFile);

    int games, gameNum, j;
    //scan in number of boggle games to be played
    scanf("%d", &games);
    int loop = games;
    for(gameNum = 0; gameNum < loop ; gameNum++){
        //output formatting
        printf("Words for Game #%d:\n", gameNum+1);

        //scans in the boggle board
        char board[4][4];
        for(i=0;i<4;i++)
            scanf("%s", board[i]);

        //initialize a used array
        int used [4][4] = { {0,0,0,0} , {0,0,0,0} , {0,0,0,0} , {0,0,0,0} };

        //string used to check potential words
        char word [17];
        //staring from every spot on the board
        for(i=0;i<4;i++)
            for(j=0;j<4;j++){
                //makes that letter your word
                word[0] = board[i][j];
                word[1] = '\0';
                used[i][j] = 1;
                //find all the words that start in that spot
                wordStartingWith(board,used,word,i,j,dict);
                //reset used array for checking the next spot
                used[i][j] = 0;

            }
    }
    //free the dictionary
    freeDict(dict);

    return 0;
}

//inserts
trie* insertWord(trie* dict, char word[], int k){
    //if new nodes need to be made, make them
    if(dict == NULL)
        dict = init();

    //if the whole word is in, set is word to 1
    if(k == strlen(word))
        dict->isWord = 1;

    //if more of the word remains, go deeper into the trie
    if(k < strlen(word))
        dict->nextLetter[word[k]-'a'] = insertWord(dict->nextLetter[word[k]-'a'], word, k+1);

    return dict;
}

//creates a blank node
trie* init(){
    int i;
    trie* node = malloc(sizeof(trie));
    node->isWord = 0;
    for( i=0 ; i<26 ; i++ ){
        node->nextLetter[i] = NULL;
    }
    return node;
}

//finds all the words starting with a partial path
void wordStartingWith(char board[4][4], int used[4][4], char word[17], int x, int y, trie* dict){
    //if the partial path is a word, print it
    if(hasWord(word, dict) == 2)
        printf("%s\n",word);
    //if there are any words starting with the letters you have so far
    if(hasWord(word, dict) > 0){
        int DX [8] = {1,1,1,0,0,-1,-1,-1};
        int DY [8] = {1,0,-1,1,-1,1,0,-1};
        int i;
        //checks all of the next possible spots
        for(i=0;i<8;i++){
            if(x+DX[i] >= 0 && x+DX[i] < 4 && y+DY[i] >= 0 && y+DY[i] < 4){
                if(!used[x+DX[i]][y+DY[i]]){
                    x += DX[i];
                    y += DY[i];
                    word[strlen(word)+1] = '\0';
                    word[strlen(word)] = board[x][y];
                    used[x][y] = 1;
                    wordStartingWith(board, used, word, x, y, dict);
                    used[x][y] = 0;
                    x -= DX[i];
                    y -= DY[i];
                    word[strlen(word)-1] = '\0';
                }
            }
        }
    }
}

//returns 2 if "word" is a word
//returns 1 if "word" is a prefix
//returns 0 otherwise
int hasWord(char* word, trie* dict){
    int i;
    for(i=0;i<strlen(word);i++){
        dict = dict->nextLetter[word[i]-'a'];
        if(dict == NULL)
            return 0;
    }
    if(dict->isWord)
        return 2;
    for(i=0;i<26;i++)
        if(dict->nextLetter[i] != NULL)
            return 1;
    return 0;
}

//frees a dictionary
void freeDict(trie* dict){
    if(dict != NULL){
        int i;
        for(i=0;i<26;i++)
            freeDict(dict->nextLetter[i]);
        free(dict);
    }
}
