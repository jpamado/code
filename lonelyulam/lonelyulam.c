#include<stdio.h>
int isqrt (int x){
    int i;
    for(i=1;1;i++)
        if((i*i)>=x)
        return i;
}
int isPrime (int x){
    if(x==2||x==3||x==5||x==7)
        return 1;
    if(x==0||x==1||x%2==0||x%3==0)
        return 0;
    int check=isqrt(x);
    if(check%2==0)
        check--;
    int prime;
    for(prime=1;prime;check-=2)
        if(x%check==0)
            prime=0;
    if (check!=-1)
        return 0;
    return 1;
}
int main() {
    int N;
    printf("N = ");
    scanf("%d", &N);
    int spiral[N][N];
    int r;
    int c;
    for (r=0;r<N;r++)
        for (c=0;c<N;c++)
            spiral[r][c]=0;
    int i;
    r=N/2;
    c=N/2;
    int direction=2;

    for(i=1;i<=N*N;i++){
        spiral[r][c]=i;
        if(direction==3){
            r--;
            if(spiral[r][c+1]==0)
                direction=0;
        }else if(direction==0){
            c++;
            if(spiral[r+1][c]==0)
                direction=1;
        }else if(direction==1){
            r++;
            if(spiral[r][c-1]==0)
                direction=2;
        }else{
            c--;
            if(spiral[r-1][c]==0)
                direction=3;
        }
    }
    for(r=0;r<N;r++)
        for(c=0;c<N;c++)
            spiral[r][c]=isPrime(spiral[r][c]);
    for(r=1;r<N-1;r++)
        for(c=1;c<N-1;c++)
            if(spiral[r][c]&&(spiral[r-1][c]||spiral[r+1][c+1]||spiral[r-1][c+1]||spiral[r+1][c-1]||spiral[r-1][c-1]))
                spiral[r][c]=2;
    for(r=0;r<N;r++){
        spiral[r][0]=0;
        spiral[r][N-1]=0;
    }
    for(c=0;c<N;c++){
        spiral[0][c]=0;
        spiral[N-1][c]=0;
    }
    for(r=0;r<N;r++)
        for(c=0;c<N;c++)
            if(spiral[r][c]==2)
                spiral[r][c]=0;
    r=N/2;
    c=N/2;
    direction=2;
    int j=1;
    for(i=1;i<=N*N;i++){
        if(spiral[r][c]==1){
            printf("f(%d)=%d\n",j,i);
            j++;
        }
        spiral[r][c]=i;
        if(i<80){
            if(direction==3){
                r--;
                if(spiral[r][c+1]==0)
                    direction=0;
            }else if(direction==0){
                c++;
                if(spiral[r+1][c]==0)
                    direction=1;
            }else if(direction==1){
                r++;
                if(spiral[r][c-1]==0)
                    direction=2;
            }else{
                c--;
                if(spiral[r-1][c]==0)
                    direction=3;
            }
        }else{
            if(direction==3){
                r--;
                if(spiral[r][c+1]<32)
                    direction=0;
            }else if(direction==0){
                c++;
                if(spiral[r+1][c]<32)
                    direction=1;
            }else if(direction==1){
                r++;
                if(spiral[r][c-1]<32)
                    direction=2;
            }else{
                c--;
                if(spiral[r-1][c]<32)
                    direction=3;
            }
        }
    }
    return 0;
}
