#include<stdio.h>

int collatzCollapse(int N);

int main() {

    int N,i;
    scanf("%d",&N);
    for(i=1;i<=N;i++)
        if(collatzCollapse(i)==i)
            printf("%d\n",i);
}

int collatzCollapse(int N){
if (N == 1)
    return 0;
if (N == 2)
    return 1;
if (N%2)
    return (collatzCollapse((3*N+1)/2) + 2);
else
    return (collatzCollapse(N/2) + 1);
}
